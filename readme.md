# A demo of integration between Django and Open ID Connect
For some more information go to [the official mozilla-django-oidc's docs](https://mozilla-django-oidc.readthedocs.io/en/stable/installation.html)

# To reproduce (we will assume Linux/MacOS with Python and virtualenv installed)
0. Prepare the directory: `mkdir -p ~/django-oidc/; cd ~/django-oidc`
1. Create and activate virtualenv: `virtualenv env; source env/bin/activate`
2. Install django and mozilla-django-oidc: `pip3 install django mozilla-django-oidc`
3. Add `'mozilla_django_oidc'` to the list of installed apps in _settings.py_
4. Add `'mozilla_django_oidc.auth.OIDCAuthenticationBackend',` to `AUTHENTICATION_BACKENDS` in _settings.py_. You might need to create this variable yourself:
```
AUTHENTICATION_BACKENDS = (
    'mozilla_django_oidc.auth.OIDCAuthenticationBackend',
    # ...
)
```
5. Fill the following fields in _settings.py_:
```
OIDC_RP_CLIENT_ID = os.environ['OIDC_RP_CLIENT_ID']
OIDC_RP_CLIENT_SECRET = os.environ['OIDC_RP_CLIENT_SECRET']
OIDC_OP_AUTHORIZATION_ENDPOINT = "<URL of the OIDC OP authorization endpoint>"
OIDC_OP_TOKEN_ENDPOINT = "<URL of the OIDC OP token endpoint>"
OIDC_OP_USER_ENDPOINT = "<URL of the OIDC OP userinfo endpoint>"
LOGIN_REDIRECT_URL = "<URL path to redirect to after login>"
LOGOUT_REDIRECT_URL = "<URL path to redirect to after logout>"
```
6. Fill routes in `urls.py`:
```
from django.urls import include, path   # Don't forget about adding `include`

urlpatterns = [
    # ...
    path('oidc/', include('mozilla_django_oidc.urls')),  # <--- add this line
    # ...
]
```
7. Add the HTML templates