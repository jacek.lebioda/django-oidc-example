from django.urls import path

from . import views


urlpatterns = [
    path(r'', views.index),
    path(r'after_login', views.after_login),
    path(r'after_logout', views.after_logout),
]
