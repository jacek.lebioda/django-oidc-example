from django.shortcuts import render


def index(request):
    return render(request, 'web/index.html', {})


def after_login(request):
    return render(request, 'web/index.html', {})


def after_logout(request):
    return render(request, 'web/index.html', {})